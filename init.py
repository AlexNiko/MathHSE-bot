import telebot
from cfg.config import *
from pprint import pprint

bot = telebot.TeleBot(token)
print("We are ready, sir\n")

import json
from datetime import *

with open('handlers/petooh/petooh_list.json') as f:
    petooh_list = json.load(f)
    f.close()
pprint(petooh_list)

with open('handlers/petooh/last_petooh.json') as f:
    last_petooh = json.load(f)
    f.close()
pprint(last_petooh)

time_to_wipe = ((datetime.timestamp(datetime.today()) - last_petooh['when']) > 864000.0)
print(time_to_wipe)

# print(bot.get_chat(mathhse_chat))

def petooh_reg(from_user):
    user = {
        'username': get_username(from_user),
        # 'tag': message.from_user.username,
        'counter': 0,
        'id': from_user.id
    }
    if user_in_list(user):
        return 'already_in'
        # bot.send_message(chat.id, "Эй, " + user['username'] + ", ты уже в списке! Ты был петухом: " +
        #                  str(get_from_list_by_id(user['id'])['counter']) + " раз", reply_markup=keyboard)
    else:
        # bot.send_message(chat.id, "Добро пожаловать в очередь, " + user['username'], reply_markup=keyboard)
        petooh_list.append(user)
        with open('handlers/petooh/petooh_list.json', 'w') as files:
            json.dump(petooh_list, files)
            files.close()
        return 'welcome'

def user_in_list(user):
    if len(petooh_list) == 0:
        return False
    else:
        for i in petooh_list:
            if i['id'] == user['id']:
                return True
            else: pass
        return False


def get_username(user):
    username = ""
    if not (user.first_name == None):
        username = username + user.first_name
        if not (user.last_name == None):
            username =  username +" "+ user.last_name
            if not (user.username == None):
                username = "@" + user.username
    return username

def get_from_list_by_id(id):
    for u in petooh_list:
        if u['id'] == id:
            return u
        else: pass

    return get_from_list_by_id(221101650)

max_id = '0'

def get_deadlines():
    global max_id
    with open('deadlines/deadlines.json') as f:
        deadlines = json.load(f)
        f.close()
    for d in deadlines:
        max_id = max(max_id, d["id"])
    # print(max_id)
    return deadlines

def add_deadline(deadline):
    cur_deadlines = get_deadlines()
    global max_id

    if not('url' in deadline.keys()):
        deadline['url'] = None

    cur_deadlines.append({'name': deadline["name"],
                          'desc': deadline["desc"],
                          'time': deadline["time"],
                          'url': deadline["url"],
                          'id': str(int(max_id) + 1)})
    max_id = str(int(max_id) + 1)
    with open('deadlines/deadlines.json', 'w') as files:
        json.dump(cur_deadlines, files)
        files.close()

def remove_deadline(deadline):
    cur_deadlines = get_deadlines()
    # print(cur_deadlines)
    cur_deadlines.remove(deadline)
    # print(cur_deadlines)
    with open('deadlines/deadlines.json', 'w') as files:
        json.dump(cur_deadlines, files)
        files.close()

def get_deadline_by_id(id):
    deadlines = get_deadlines()
    for d in deadlines:
        if d["id"] == id:
            return d
    return False

def true_wipe():
    global time_to_wipe
    print("Making True \n")
    print(time_to_wipe)
    time_to_wipe = True
    print(time_to_wipe)
    print("VJUH! \n")

def false_wipe():
    global time_to_wipe
    print("Making False \n")
    print(time_to_wipe)
    time_to_wipe = False
    print(time_to_wipe)
    print("VJUH! \n")

def get_wipe():
    return time_to_wipe