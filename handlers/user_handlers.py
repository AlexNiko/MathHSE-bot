# -*- coding: utf-8 -*-
from init import *
import random
from telebot import types
from vk import *

@bot.message_handler(commands=['petooh'])
def cmd_petooh(message):
    if message.chat.id == mathhse_chat:
        cmd_time = datetime.timestamp(datetime.today())
        if (len(petooh_list) == 0):
            bot.send_message(message.chat.id, "Пока никто не хочет быть петухом. Все правильно?")
        elif ((cmd_time - last_petooh['when']) > 86400.0) or (last_petooh['who'] == None) or get_wipe():
            last_petooh['when'] = cmd_time
            cmd_who = random.choice(petooh_list)
            random.shuffle(petooh_list)
            last_petooh['who'] = cmd_who
            bot.send_message(message.chat.id, "Вот это да! У нас новый _петух дня_! Это: "
                             + cmd_who['username']+" ", parse_mode='markdown')
            cmd_who['counter'] = cmd_who['counter'] + 1
            with open('handlers/petooh/last_petooh.json', 'w') as files:
                json.dump(last_petooh, files)
                files.close()
            false_wipe()
        else:
            bot.send_message(message.chat.id,
                             "Псс, будьте аккуратнее, ведь _петух дня_: *"
                             + last_petooh['who']['username']+'*', parse_mode='markdown')
    else:
        bot.send_message(message.chat.id, 'Прости, пупсик, этот бот только для Math HSE Group')


@bot.message_handler(commands=['petoohreg'])
def cmd_petoohreg(message):
    keyboard = types.InlineKeyboardMarkup()
    callback_button = types.InlineKeyboardButton(text="Я тоже хочу!", callback_data="petoohreg")
    keyboard.add(callback_button)
    if message.chat.id == mathhse_chat:
        if (petooh_reg(message.from_user) == 'welcome'):
            bot.send_message(message.chat.id, "Добро пожаловать в очередь, *" + get_username(message.from_user)+'*',
                             reply_markup=keyboard, parse_mode='markdown')
        elif (petooh_reg(message.from_user) == 'already_in'):
            bot.send_message(message.chat.id,
                             "Эй, " + get_username(message.from_user) + ", ты уже в списке! Ты был петухом: " +
                             str(get_from_list_by_id(message.from_user.id)['counter']) + " раз", reply_markup=keyboard)
    else:
        bot.send_message(message.chat.id, 'Прости, пупсик, этот бот только для Math HSE Group')

@bot.message_handler(commands=['deadlines'])
def cmd_deadline(message):
    deadlines = get_deadlines()
    if len(deadlines) == 0:
        bot.send_message(message.chat.id, "Радуйся или плачь, но я не нашел текущих дедлайннов")
    else:
        keyboard = types.InlineKeyboardMarkup()
        for d in deadlines:
            # print(d["id"])
            ddl_button = types.InlineKeyboardButton(text=d["name"], callback_data=d["id"])
            keyboard.add(ddl_button)
        bot.send_message(message.chat.id, "Бежать уже поздно, вот список дедлайнов", reply_markup=keyboard)
    bot.send_message(ackbar_chat_id, '[' + get_username(message.from_user) + '] _triggers deadline cmd_',
                     parse_mode='markdown')

import textwrap

@bot.message_handler(commands=['news'])
def cmd_news(message):
    news = get_news(3)
    print(news)
    if len(news) == 0:
        bot.send_message(message.chat.id, 'Прости, друг, у меня нет для тебя новостей :c')
    else:
        keyboard = types.InlineKeyboardMarkup()
        i = 1
        ans = ''
        for n in news:
            ans = ans + 'Новость #' + str(i) + ' ' \
                  + textwrap.shorten(n['title'], width=50, placeholder="...") \
                  + '\nПодробнее тут: ' + n['url'] + '\n\n'
            n_button = types.InlineKeyboardButton(text="Новость #" + str(i), callback_data=n['url'])
            keyboard.add(n_button)
            i = i + 1
        bot.send_message(message.chat.id, ans, reply_markup=keyboard, disable_web_page_preview=True)
    bot.send_message(ackbar_chat_id, '[' + get_username(message.from_user) + '] _triggers news cmd_',
                     parse_mode='markdown')