# -*- coding: utf-8 -*-
from handlers.admin_handlers import *
from handlers.user_handlers import *
from vk import url
from telebot import types

@bot.callback_query_handler(func=lambda call: True)
def callback_inline(call):
    # Если сообщение из чата с ботом
    if call.message:
        if call.data == "petoohreg":
            if (petooh_reg(call.from_user) == 'welcome'):
                bot.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Добро пожаловать в очередь!")
            elif (petooh_reg(call.from_user) == 'already_in'):
                bot.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Эй, " + get_username(call.from_user) + ", ты уже в списке! Ты был петухом: " +
                             str(get_from_list_by_id(call.from_user.id)['counter']) + " раз")
            bot.send_message(ackbar_chat_id, '[' + get_username(call.from_user) + '] _registers for petooh-game_',
                             parse_mode='markdown')

        elif (call.data.find("remove=") == 0) and data_in_deadline(call.data[7:]):
            remove_deadline(get_deadline_by_id(call.data[7:]))
            bot.answer_callback_query(callback_query_id=call.id, show_alert=True, text="Делайн удален, поздравляю!")

        elif (call.data.find(url) == 0):
            n =find_new_by_url(call.data)
            if n == 0:
                print('VSE IDET PO PIZDE')
            else:
                kk = types.InlineKeyboardMarkup()
                n_button = types.InlineKeyboardButton(text="Открыть новость", url=n['url'])
                kk.add(n_button)
                bot.send_message(call.message.chat.id, n['title'] + '\n' + n['url'],
                                 disable_web_page_preview=False,
                                 reply_markup=kk)
                bot.send_message(ackbar_chat_id, '[' + get_username(call.from_user) + '] _triggers new_',
                                 parse_mode='markdown',reply_markup=kk)
        elif data_in_deadline(call.data):
            deadline = get_deadline_by_id(call.data)
            if deadline:
                answer = ""
                if not (deadline["name"] == None):
                    answer = answer + "*" + deadline["name"] + "*\n"
                if not (deadline["desc"] == None):
                    answer = answer + "_" + deadline["desc"] + "_\n"
                if not (deadline["time"] == None):
                    answer = answer + "До " + deadline["time"] + "\n"
                if not (deadline["url"] == None):
                    answer = answer + "См. тут: " + deadline["url"]
                bot.send_message(call.message.chat.id, answer, parse_mode='markdown')
                bot.send_message(ackbar_chat_id, '['+ get_username(call.from_user)+'] _triggers deadline_: ' + deadline["name"], parse_mode='markdown')




def data_in_deadline(data):
    deadlines = get_deadlines()
    for d in deadlines:
        if d["id"] == data:
            return True
    return False