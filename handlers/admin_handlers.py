# -*- coding: utf-8 -*-
from init import *

@bot.message_handler(commands=['wipe'])
def cmd_wipe(message):
    if message.chat.id == ackbar_chat_id:
        bot.send_message(message.chat.id, "_Таймер петуха обнулен!_", parse_mode='markdown')
        true_wipe()

@bot.message_handler(commands=['stat'])
def cmd_stat(message):
    if message.chat.id == ackbar_chat_id:
        bot.send_message(message.chat.id, '*Last petooh* ' +
                         str(last_petooh) +
                         '\n*Number want-to-be petoohs*: '
                         + str(len(petooh_list))
                         + ' to know more: /getpetoohlist'
                         +'\n*Time to wipe:* ' + str(get_wipe()), parse_mode='markdown')

@bot.message_handler(commands=['getpetoohlist'])
def cmd_get_petooh_list(message):
    if message.chat.id == ackbar_chat_id:
        bot.send_message(message.chat.id, '`' + str(petooh_list) + '`')
        with open('handlers/petooh/petooh_list.json', 'w') as files:
            json.dump(petooh_list, files)
            files.close()

@bot.message_handler(commands=['adddeadline'])
def cmd_add_deadline(message):
    if message.chat.id == ackbar_chat_id:
        command = message.text.split(' ')
        if len(command) == 1:
            bot.send_message(message.chat.id,
                             'Введите больше аргументов! `name=\'\' desc=\'\' time=\'\' url=\'optional\'`',
                             parse_mode='markdown')
        else:
            command = deadline_input(command[1:])
            if command:
                add_deadline(command)
                bot.send_message(message.chat.id,
                                 '*Дедлайн добавлен!* Земля тебе пухом, братишка',
                                 parse_mode='markdown')
                # print(command)
            else:
                bot.send_message(message.chat.id,
                                 'Введите больше аргументов! `name=\'\' desc=\'\' time=\'\' url=\'optional\'`',
                                 parse_mode='markdown')


def deadline_input(list):
    lul = {}
    for i in list:
        if i.find("name=") == 0:
            lul["name"] = i[5:]
        elif i.find("desc=") == 0:
            lul["desc"] = i[5:]
        elif i.find("time=") == 0:
            lul["time"] = i[5:]
        elif i.find("url=") == 0:
            lul["url"] = i[4:]
    return lul

from telebot import types

@bot.message_handler(commands=['removedeadline'])
def cmd_remove_deadline(message):
    deadlines = get_deadlines()
    if len(deadlines) == 0:
        bot.send_message(message.chat.id, "Радуйся или плачь, но я не нашел текущих дедлайннов")
    else:
        keyboard = types.InlineKeyboardMarkup()
        for d in deadlines:
            # print(d["id"])
            ddl_button = types.InlineKeyboardButton(text=d["name"], callback_data="remove="+d["id"])
            keyboard.add(ddl_button)
        bot.send_message(message.chat.id, "Выбери дедлайн, который пошел или надо удалить", reply_markup=keyboard)