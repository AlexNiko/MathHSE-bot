# -*- coding: utf-8 -*-
import vk_api
from datetime import datetime

url = 'https://vk.com/wall-149818630_'

def get_news(c):
    """ Пример использования longpoll
        https://vk.com/dev/using_longpoll
        https://vk.com/dev/using_longpoll_2
    """

    login, password = '+79163798211', '6yhn^YHN'
    vk_session = vk_api.VkApi(login, password)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()

    responce = vk.wall.get(domain='mathhse17',count=c,extended=1,filter='owner')
    news = []

    now = datetime.timestamp(datetime.now())
    for p in responce['items']:
        if not ('is_pinned' in p.keys()):
            if (now - p['date'] < 2*24*60*60):
                n = {}
                if not (len(p['text']) == 0):
                    n['title'] = p['text']
                elif 'copy_history' in p.keys():
                    n['title'] = p['copy_history'][0]['text']
                n['id'] = p['id']
                n['url'] = url+str(p['id'])
                news.append(n)
    return news

def find_new_by_url(u):
    news = get_news(6)
    id = u[len(url):]
    for n in news:
        if n['id'] == int(id):
            # print(n)
            return n
    return 0